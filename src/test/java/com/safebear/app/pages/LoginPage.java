package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class LoginPage {

    WebDriver driver;

    @FindBy(id = "myid")
    WebElement userName;

    @FindBy(id = "mypass")
    WebElement userPassword;

    @FindBy(xpath = "//*[@class='btn btn-lg btn-success']")
    WebElement signInButton;

    public LoginPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkCorrectPage()
    {
        return driver.getTitle().contains("Sign");
    }

    public boolean login(UserPage userPage, String userName, String password)
    {
       this.userName.sendKeys(userName);
       userPassword.sendKeys(password);
        signInButton.submit();
        return userPage.checkCorrectPage();
    }

}
