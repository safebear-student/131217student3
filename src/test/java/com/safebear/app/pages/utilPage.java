package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 14/12/2017.
 */
public class utilPage {

    WebDriver driver;

    @FindBy(linkText = "Logout")
    WebElement logOutLink;

    public utilPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkLogOutLink()
    {
        System.out.println(driver.getTitle().contains("Logged"));


            return logOutLink.isDisplayed();

    }

    public boolean logOut()
    {
        logOutLink.click();
        return true;
    }


}
