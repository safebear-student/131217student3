package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.pages.utilPage;
import com.safebear.app.utils.utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class BaseTest {

    WebDriver driver;
    utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    utilPage utilpage;

    @Before
    public void setUp() {
        // write code to setup
        utility = new utils();
        this.driver = utility.getDriver();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        utilpage = new utilPage(driver);

        driver.get(utility.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() throws InterruptedException {
        // write code to tear down the test
        if(utility.sleepStatus())
        TimeUnit.SECONDS.sleep(Long.parseLong(utility.getSleeptime()));
        if(utilpage.checkLogOutLink())
            Thread.sleep(3000);
        driver.quit();

    }
}