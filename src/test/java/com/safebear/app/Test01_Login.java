package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin()
    {
        // write code
        // Step 1
        assertTrue(welcomePage.checkCorrectPage());

        // Step 2
        assertTrue(welcomePage.clickOnLink(this.loginPage));
        // Step 3
        assertTrue(loginPage.login(userPage,"testuser", "testing"));



    }

}


