package com.safebear.app.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class utils {

    private WebDriver driver;
    private String url;
    private String browser;
    private Properties prop = new Properties();
    InputStream input = null;
    private String sleepStaus;
    private String sleeptime;

    public utils()
    {
        try
        {
            String file = "config.properties";
            input = utils.class.getClassLoader().getResourceAsStream(file);
            if (input==null)
            {
                System.out.println("unable to load file");
                return;
            }
            prop.load(input);
            this.url = prop.getProperty("url");
            this.browser = prop.getProperty("browser");
            this.sleepStaus = prop.getProperty("sleepStatus");
            this.sleeptime = prop.getProperty("sleepTime");
        }catch(Exception e)
        {
            e.printStackTrace();
        }finally {
            if (input!=null)
            try
            {
                input.close();
            }catch(IOException er)
            {
                er.printStackTrace();
            }
        }

        switch (browser) {
            case "chrome":
                this.driver = new ChromeDriver();
                return;
            case "ie":
                this.driver = new InternetExplorerDriver();
                return;
            case "chrome_headless":
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("headless");
                this.driver =new ChromeDriver(chromeOptions);
                return;
             default:
                this.driver = new ChromeDriver();
                return;


        }
    }
    public WebDriver getDriver()
    {
        return driver;
    }

    public String getUrl()
    {
        return url;
    }
public boolean sleepStatus()
{
    if(sleepStaus.contains("on"))
        return true;
        else
            return false;

}
public String getSleeptime()
{
    return sleeptime;
}

}
